"""Please enter permutation. example [2,3,4,5,1] [8,3,1,4,5,2,6,9,7,10,12,14,13,11,15]
We found a solution
Input ....[8, 3, 1, 4, 5, 2, 6, 9, 7, 10, 12, 14, 13, 11, 15]
Move 1 ...[8, 3, 1, 4, 5, 2, 6, 7, 9, 10, 12, 14, 13, 11, 15]
Move 2 ...[8, 3, 1, 4, 5, 2, 6, 7, 9, 10, 12, 11, 13, 14, 15]
Move 3 ...[8, 3, 1, 4, 5, 2, 6, 7, 9, 10, 11, 12, 13, 14, 15]
Move 4 ...[8, 1, 3, 4, 5, 2, 6, 7, 9, 10, 11, 12, 13, 14, 15]
Move 5 ...[8, 1, 5, 4, 3, 2, 6, 7, 9, 10, 11, 12, 13, 14, 15]
Move 6 ...[8, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15]
Move 7 ...[8, 7, 6, 5, 4, 3, 2, 1, 9, 10, 11, 12, 13, 14, 15]
Move 8 ...[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
The number of nodes expanded were 833
 
 
This function will check a given node and check to see if it is finished. Finished means sorted
from smallest value to biggest.
"""
def isDone(node):
    numbItems = len(node)
    isSorted = False
    for a in range(numbItems-1):
        if node[a] <= node[a+1]:
            isSorted = True
        else:
            return False
    return isSorted
"""
This function takes in a list of nodes and returns the number of
breakpoints for each node
"""
def numOfBreakpoints(list):
    breakpoints = len(list)-1
    locationBreakPoints = []
    for i in range(len(list)-1):
        if abs(list[i] -list[i+1]) != 1:
            locationBreakPoints.append([i,i+1])
    return locationBreakPoints
"""
This function will take a stack and return the node with the smallest
number of breakpoints
"""
def minimumNode(stack):
    numItems = len(stack)
    minNode = len(stack[0])-1
    foundAt = 0
    for a in range(numItems):
        breakingPoints = numOfBreakpoints(stack[a])
        if len(breakingPoints) < minNode:
            minNode = len(breakingPoints)
            foundAt = a
    return stack[foundAt]
"""
This function will take a node and return its reverse version
[1,2,3] will return [3,2,1]
"""
def reverseList(list):
    temp = []
    maxNum = len(list)
    for i in range(len(list)):
        temp.append(i)
    for a in range(len(list)):
        temp[a]=list[maxNum-1]
        maxNum = maxNum -1
    return temp
"""
This function will return a stack with all the possible children for a
particular node
"""
def childrenOfNode(node):
    stack = []
    mini = []
    newNode = []
    num = 2;
    maxLength = len(node)
    n = maxLength
    for x in range(maxLength-1):
        if num >= 2:
            for a in range(n-1):
                mini = []
                temp = []
                for w in range(num):
                    temp.append(node[a+w])
                mini = reverseList(temp)
                newNode = []
                for m in range(len(node)):
                    newNode.append(node[m])
                for b in range(len(mini)):
                    newNode[a+b] = mini[b]            
                stack.append(newNode)
        n = n - 1;
        num = num + 1 
    return stack
 

stack = []
open = []
node = []
nodeDepth = 0
temp = []
expandedNodes = 0
node = input("Please enter permutation. example [2,3,4,5,1] ")
stack.append(node)
foundSolution = False
while len(stack) != 0:
    node1 = []
    node1 = minimumNode(stack)
    if(isDone(node1)):
        foundSolution = True
        open.append(node1)
        expandedNodes = len(stack)
        stack [:] = []
    else:
        open.append(node1)
        stack.remove(node1)
        temp = childrenOfNode(node1)
        stack.extend(childrenOfNode(node1))
        
if(foundSolution):
    print "We found a solution"
    print "Input ....%s"% (node)
    for i in range(len(open)-1):
        print "Move %d ...%s"% (i+1, open[i+1])
    print "The number of nodes expanded were %d"% (expandedNodes)
 
