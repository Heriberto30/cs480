table = [[6,8,4,9,7,3,2,1,5],
         [0,0,0,5,1,0,0,0,0],
         [0,5,2,0,0,0,0,9,0],
         [1,0,0,6,0,0,4,3,0],
         [0,9,0,0,0,0,0,8,0],
         [0,7,6,0,0,8,0,0,9],
         [0,4,0,0,0,0,1,2,0],
         [0,0,0,0,9,1,0,0,0],
         [0,0,1,2,8,0,0,0,3]]

table2 = [[8,5,0,0,0,2,4,0,0],
          [7,2,0,0,0,0,0,0,9],
          [0,0,4,0,0,0,0,0,0],
          [0,0,0,1,0,7,0,0,2],
          [3,0,5,0,0,0,9,0,0],
          [0,4,0,0,0,0,0,0,0],
          [0,0,0,0,8,0,0,7,0],
          [0,1,7,0,0,0,0,0,0],
          [0,0,0,0,3,6,0,4,0]]


"""
This function will return the number of available spots for
new candidates. This function will need an existing 9X9 Sudoku
as input
"""
def numberofzeros(table):
    numbzeros = 0
    for a in range(9):
        for b in range(9):
            if table[a][b]== 0:
                numbzeros = numbzeros + 1
    return numbzeros
"""
This function will return the next available spot for
candidates if no spot is found it returns -1, -1 otherwise it returns
an array of cordinates. This function will need an existing 9X9 Sudoku
as input
"""
def firstZero(table):
    i = -1;
    j = -1;
    cords=[];
    for a in range(9):
        for b in range(9):
            if table[a][b] == 0:
                i = a
                j = b
                cords.append(i)
                cords.append(j)
                return cords;
    cords.append(i)
    cords.append(j)
    return cords
"""
This function returns a list of available candidates
for the current spot. Parameters are 9x9 matrix, a row and column
"""
def candidates(table, row, col):
    d=[]
    c = [1,2,3,4,5,6,7,8,9]
    if table[row][col] != 0:
        return
    for i in range(9):
        if table[row][i] != 0:
            d.append(table[row][i])
    for i in range(9):
        if table[i][col] != 0:
            d.append(table[i][col])
    if row % 3 == 0 and col % 3 == 0:
        d.append(table[row+1][col+1])
        d.append(table[row+1][col+2])
        d.append(table[row+2][col+1])
        d.append(table[row+2][col+2])
    if row % 3 == 1 and col % 3 == 0:
        d.append(table[row-1][col+1])
        d.append(table[row-1][col+2])
        d.append(table[row+1][col+1])
        d.append(table[row+1][col+2])
    if row % 3 == 2 and col % 3 == 0:
        d.append(table[row-2][col+1])
        d.append(table[row-2][col+2])
        d.append(table[row-1][col+1])
        d.append(table[row-1][col+2])
    if row % 3 == 0 and col % 3 == 1:
        d.append(table[row+1][col-1])
        d.append(table[row+2][col-1])
        d.append(table[row+1][col+1])
        d.append(table[row+2][col+1])
    if row % 3 == 1 and col % 3 == 1:
        d.append(table[row-1][col-1])
        d.append(table[row+1][col-1])
        d.append(table[row-1][col+1])
        d.append(table[row+1][col+1])
    if row % 3 == 2 and col % 3 == 1:
        d.append(table[row-2][col-1])
        d.append(table[row-1][col-1])
        d.append(table[row-2][col+1])
        d.append(table[row-1][col+1])
    if row % 3 == 0 and col % 3 == 2:
        d.append(table[row+1][col-1])
        d.append(table[row+1][col-2])
        d.append(table[row+2][col-1])
        d.append(table[row+2][col-2])
    if row % 3 == 1 and col % 3 == 2:
        d.append(table[row-1][col-2])
        d.append(table[row-1][col-1])
        d.append(table[row+1][col-2])
        d.append(table[row+1][col-1])
    if row % 3 == 2 and col % 3 == 2:
        d.append(table[row-2][col-2])
        d.append(table[row-2][col-1])
        d.append(table[row-1][col-2])
        d.append(table[row-1][col-1])
        
    unique = set(d)
    temp = list(unique)
    if temp[0] == 0:
        temp.remove(0)
    p = 0
    b = 0
    for i in range(9):
        if b > len(temp)-1:
            ""
        elif temp[b] == c[p]:
            c.remove(c[p])
            b = b + 1
        else:
            p = p + 1
    return c
"""
This function will take a sudoku board(table) 9x9 and display if a solution is found
If such solution exists, it will print the solution; otherwise No solution is displayed
"""
def findSolution(table2):
    stack = []
    winningBoard = []
    stack.append(table2)
    print "Initial Sudoku"
    for p in range(9):
        print table2[p]
    while stack != []:
        winningBoard = stack[0]
        stack[0:1] = []
        if numberofzeros(winningBoard) == 0:
            print "We found a solution"
        else:
            cands = []
            cords = firstZero(winningBoard)
            
            if cords[0] != -1 and cords[1] != -1:
                cands = candidates(winningBoard, cords[0], cords[1])
                for p in range(len(cands)):
                    table1 = [[0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0],
                          [0,0,0,0,0,0,0,0,0]]
                    for a in range(9):
                        for b in range(9):
                            table1[a][b] = winningBoard[a][b]
                    table1[cords[0]][cords[1]] = cands[p]
                    stack.append(table1)
            else:
                print "no soluction"
    if numberofzeros(winningBoard) > 0:
        print "No solution found."
    else:
        for z in range(9):
            print winningBoard[z]
print "All indices are based 0"
print "The first zero is found at"
print firstZero(table)
print ""
print "candidates for (2,6) are"
print candidates(table, 2, 6)
print ""
print "candiates for (1,5) are"
print candidates(table, 1, 5)
print ""
findSolution(table2)