"""
This function will check a given node and check to see if it is finished. Finished means sorted
from smallest value to biggest.
"""
def isDone(node):
    numbItems = len(node)
    isSorted = False
    for a in range(numbItems-3):
        if node[a] <= node[a+1]:
            isSorted = True
        else:
            return False
    return isSorted


"""
This function takes in a list of nodes and returns the number of
breakpoints for each node
"""
def numOfBreakpoints(list):
    breakpoints = len(list)-3
    locationBreakPoints = []
    for i in range(len(list)-3):
        if abs(list[i] -list[i+1]) != 1:
            locationBreakPoints.append([i,i+1])
    return locationBreakPoints


"""
This function will take a stack and return the node with the smallest
number of breakpoints
"""

def minimumNode(stack):
    numItems = len(stack)
    minNode = float(len(stack[0])-3)+float(stack[0][len(stack[0])-2])
    foundAt = 0
    for a in range(numItems):
        breakingPoints = float(len(numOfBreakpoints(stack[a])))*.5+float(stack[a][len(stack[a])-2])
        if breakingPoints < minNode:
            minNode = breakingPoints
            foundAt = a
    return stack[foundAt]
"""
This function will take a node and return its reverse version
[1,2,3] will return [3,2,1]
"""
def reverseList(list):
    temp = []
    tempNode = list[len(list)-1]
    maxNum = len(list)
    for i in range(len(list)):
        temp.append(i)
    for a in range(len(list)):
        temp[a]=list[maxNum-3]
        maxNum = maxNum -1
    temp[len(list)-2] = list[len(list)-2]
    temp.pop()
    temp.append(tempNode)
    return temp


"""
This function will return a stack with all the possible children for a
particular node
"""
def childrenOfNode(node):
    stack = []
    mini = []
    newNode = []
    num = 2
    maxLength = len(node)-2 
    n = maxLength
    for x in range(maxLength-1):
        if num >= 2:
            for a in range(n-1):
                mini = []
                temp = []
                for w in range(num):
                    temp.append(node[a+w])
                temp.append(node[len(node)-2])
                temp.append(node[len(node)-2])

                mini = reverseList(temp)
                mini.pop()
                mini.pop()
                newNode = []
                for m in range(len(node)):
                    newNode.append(node[m])
                newNode.pop()
                newNode.pop()
                for b in range(len(mini)):
                    newNode[a+b] = mini[b]
                newNode.append(node[len(node)-2]+1)
                tempNode = []
                for a in range(len(node)-1):
                    tempNode.append(node[a])
                
                tempNode.pop()
                newNode.append(tempNode)
                stack.append(newNode)
        n = n - 1
        num = num + 1 
    return stack

"""def findPath(stack):
    solution = stack[len(stack)-1]
    parentOfSolution = solution[len(solution)-1]
    stackSolution = []
    stackSolution.append(solution)
  """  
def isSameNode(new, old):
    if len(new) != len(old):
        return False;
    sameNode = True;
    for i in range(len(new)-3):
        if new[i] != old[i]:
            return False
    return sameNode




stack = []
open = []
node = []
nodeDepth = 0
temp = []
expandedNodes = 0
node = input("Please enter permutation. example [2,3,4,5,1] ")
node.append(0)
tempNode = []
for a in range(len(node)-1):
    tempNode.append(0)
node.append(tempNode)
stack.append(node)
foundSolution = False
while len(stack) != 0:
    node1 = []
    node1 = minimumNode(stack)
   
    if(isDone(node1)):
        foundSolution = True
        open.append(node1)
        stack [:] = []
    else:
        open.append(node1)
        stack.remove(node1)
        stack.extend(childrenOfNode(node1))
    newSet = set(stack)
    nesList = list(stack)
    stack = []
    stack = nesList
if(foundSolution):
    print "We found a solution"
    print "Input ....%s"% (node)
    for i in range(len(open)-1):
        print "Move %d....%s"%(i+1, open[i+1])
    print "The number of nodes expanded were %d"% (expandedNodes)



