
node = ['B','B','B',
        'B','B','B',
        'B','B','B','I', 'm']

node2 = ['B','B','B',
        'B','B','B',
        'B','B','B','I','M']




def childrenOfNode(node, turn):
    stack = []
    spotSlots = []
    availableSpots = spotsOpen(node)

    for b in range(len(node)):
        if node[b] == 'B':
            spotSlots.append(b)
    
    for c in range(availableSpots):
        newChild = []
        for a in range(len(node)):
            newChild.append(node[a])
        
        if turn == 'X':
            newChild[spotSlots[0]] = 'X'
            newChild[len(newChild)-1] = 'M'
            newChild[len(newChild)-2] = 'I'
            stack.append(newChild)
            spotSlots.remove(spotSlots[0])
        elif turn == 'O':
            newChild[spotSlots[0]] = 'O'
            newChild[len(newChild)-1] = 'm'
            newChild[len(newChild)-2] = 'I'
            stack.append(newChild)
            spotSlots.remove(spotSlots[0])
                
    return stack


         
def spotsOpen(node):
    openSpots = 0
    for i in range(len(node)-1):
        if node[i] == 'B':
            openSpots = openSpots + 1

    return openSpots

def isDone(node):
    
    if spotsOpen(node) == 0:
        return True
    return False


def numOfLines(node):

    points = 0
    pointsO = 0
    num = 0
    row = 0
    col = 0

    for i in range(len(node)-1):
        if node[i] != 'O':
            num = num + 1
        if num == 3 and i % 3 == 2:
            points = points + 1
            num = 0
        if num != 3 and i % 3 == 2:
            num = 0
    for i in range(len(node)-1):
        if col > 6:
            col = 0
            row = row + 1
            num = 0
        if node[col+row] != 'O':
            num = num + 1 
        if num == 3:
            points = points + 1
        col = col + 3
    num = 0
    row = 0
    col = 0
    for i in range(len(node)-1):
        if node[i] != 'X':
            num = num + 1
        if num == 3 and i % 3 == 2:
            pointsO = pointsO + 1
            num = 0
        if num != 3 and i % 3 == 2:
            num = 0
    for i in range(len(node)-1):
        if col > 6:
            col = 0
            row = row + 1
            num = 0
        if node[col+row] != 'X':
            num = num + 1 
        if num == 3: 
            pointsO = pointsO + 1
        col = col + 3
        
    if node[0] != 'O' and node[4] != 'O' and node[8] != 'O':
        points = points + 1;
    if node[2] != 'O' and node[4] != 'O' and node[6] != 'O':
        points = points + 1;
    if node[0] != 'X' and node[4] != 'X' and node[8] != 'X':
        pointsO = pointsO + 1
    if node[2] != 'X' and node[4] != 'X' and node[6] != 'X':
        pointsO = pointsO + 1
    return points-pointsO

def minOrMax(node):
    n = spotsOpen(node)
    player = 'm'
    if n == 9 or n == 6 or n == 4 or n == 2 or n == 0:
        player = 'M'
    return player


def minValue(node1, node2):
    if node1 < node2:
        return node1
    else:
        return node2

def maxValue(node1, node2):
    if node1 > node2:
        return node1
    else:
        return node2

def sameNode(node1, node2):
    same = True
    for i in range(len(node1)-2):
        if node1[i] != node2[i]:
            return False
    return same


stack = []
stack.append(node)
turn = 'X'
expansion = False
value = 0
turns = 0
while turns < 30:
    
    node1 = stack[0]
    
    player = minOrMax(node1)
    if player == 'm':
        turn = 'O'
    else:
        turn = 'X'
    if sameNode(node,node1) and node1[len(node1)-2] != 'I':
        value = node1[len(node1)-1]
        print "!!!!!!!!!!!WE HAVE A VALUE IN THE ROOT!!!!!!!!"
    if node1[len(node1)-2] != 'I':
        "print found one with a value assigned"
        n = spotsOpen(node1)
        n = n + 1
        foundParent = False
        for i in range(len(stack)):
            n2 = spotsOpen(stack[i])
            if n2 == n and foundParent == False:
       
                foundParent = True
                if stack[i][10] == 'M':
                    if stack[i][9] < node1[9] or stack[i][9] == 'I':
                        stack[i][9] = node1[9]
                else:
                    if stack[i][9] > node1[9] or stack[i][9] == 'I':
                        stack[i][9] = node1[9]
            
        stack.remove(node1)
    number = spotsOpen(node1)
    if node1[len(node1)-2] == 'I' and number == 4:
        node1[len(node1)-2] = numOfLines(node1)
    else:
        newChildren = childrenOfNode(node1, turn)
        print "newChildren of %s "%(node1)
        print newChildren
        numOfChildren = len(newChildren)
        for i in range(len(newChildren)):
            stack.insert(0, newChildren[numOfChildren-1])
            numOfChildren = numOfChildren - 1
        
    turns = turns + 1
